class CommentsController < ApplicationController
  before_action :set_tweet
  before_action :set_tweet_and_comment, only: [:show, :edit, :update, :destroy]

  def new
    @comment = @tweet.comments.new
  end

  def show
  end

  def create
    @comment = @tweet.comments.create!(comment_params)

    respond_to do |format|
      format.turbo_stream
      format.html { redirect_to @tweet}
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @tweet, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @tweet.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_tweet
    @tweet = Tweet.find(params[:tweet_id])
  end

  def set_tweet_and_comment
    # @tweet = Tweet.find(params[:tweet_id])
    @comment = @tweet.comments.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:body)
  end
end
