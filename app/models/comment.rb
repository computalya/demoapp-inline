class Comment < ApplicationRecord
  belongs_to :tweet
  has_rich_text :body

  broadcasts_to :tweet
end
