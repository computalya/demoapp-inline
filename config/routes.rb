Rails.application.routes.draw do
  resources :tweets do
    # resources :comments, only: [:new, :create, :edit]
    resources :comments
  end

  root to: 'tweets#index'
end
